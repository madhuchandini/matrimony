<div class="pull-right">
	<a href="<?php echo site_url('state/add'); ?>" class="btn btn-success">Add</a> 
</div>

<table class="table table-striped table-bordered">
    <tr>
		<th>ID</th>
		<th>Country</th>
		<th>Name</th>
		<th>Actions</th>
    </tr>
	<?php foreach($states as $S){ ?>
    <tr>
		<td><?php echo $S['id']; ?></td>
		<td><?php echo $S['country']; ?></td>
		<td><?php echo $S['name']; ?></td>
		<td>
            <a href="<?php echo site_url('state/edit/'.$S['id']); ?>" class="btn btn-info btn-xs">Edit</a> 
            <a href="<?php echo site_url('state/remove/'.$S['id']); ?>" class="btn btn-danger btn-xs">Delete</a>
        </td>
    </tr>
	<?php } ?>
</table>
