<div class="pull-right">
	<a href="<?php echo site_url('city/add'); ?>" class="btn btn-success">Add</a> 
</div>

<table class="table table-striped table-bordered">
    <tr>
		<th>ID</th>
		<th>State</th>
		<th>Name</th>
		<th>Actions</th>
    </tr>
	<?php foreach($cities as $C){ ?>
    <tr>
		<td><?php echo $C['id']; ?></td>
		<td><?php echo $C['state']; ?></td>
		<td><?php echo $C['name']; ?></td>
		<td>
            <a href="<?php echo site_url('city/edit/'.$C['id']); ?>" class="btn btn-info btn-xs">Edit</a> 
            <a href="<?php echo site_url('city/remove/'.$C['id']); ?>" class="btn btn-danger btn-xs">Delete</a>
        </td>
    </tr>
	<?php } ?>
</table>
<div class="pull-right">
    <?php echo $this->pagination->create_links(); ?>    
</div>
