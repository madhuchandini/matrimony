<?php echo form_open('fixedlistvalue/add',array("class"=>"form-horizontal")); ?>

	<div class="form-group">
		<label for="typeId" class="col-md-4 control-label">TypeId</label>
		<div class="col-md-8">
			<input type="text" name="typeId" value="<?php echo $this->input->post('typeId'); ?>" class="form-control" id="typeId" />
		</div>
	</div>
	<div class="form-group">
		<label for="value" class="col-md-4 control-label">Value</label>
		<div class="col-md-8">
			<input type="text" name="value" value="<?php echo $this->input->post('value'); ?>" class="form-control" id="value" />
		</div>
	</div>
	
	<div class="form-group">
		<div class="col-sm-offset-4 col-sm-8">
			<button type="submit" class="btn btn-success">Save</button>
        </div>
	</div>

<?php echo form_close(); ?>