<div class="pull-right">
	<a href="<?php echo site_url('fixedlistvalue/add'); ?>" class="btn btn-success">Add</a> 
</div>

<table class="table table-striped table-bordered">
    <tr>
		<th>ID</th>
		<th>TypeId</th>
		<th>Value</th>
		<th>Actions</th>
    </tr>
	<?php foreach($fixedlistvalues as $F){ ?>
    <tr>
		<td><?php echo $F['id']; ?></td>
		<td><?php echo $F['typeId']; ?></td>
		<td><?php echo $F['value']; ?></td>
		<td>
            <a href="<?php echo site_url('fixedlistvalue/edit/'.$F['id']); ?>" class="btn btn-info btn-xs">Edit</a> 
            <a href="<?php echo site_url('fixedlistvalue/remove/'.$F['id']); ?>" class="btn btn-danger btn-xs">Delete</a>
        </td>
    </tr>
	<?php } ?>
</table>
