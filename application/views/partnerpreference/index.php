<div class="pull-right">
	<a href="<?php echo site_url('partnerpreference/add'); ?>" class="btn btn-success">Add</a> 
</div>

<table class="table table-striped table-bordered">
    <tr>
		<th>User Id</th>
		<th>Agefrom</th>
		<th>Ageto</th>
		<th>Heightfrom</th>
		<th>Heightto</th>
		<th>Marital Status</th>
		<th>Health Information</th>
		<th>Skintone</th>
		<th>Diet</th>
		<th>Smoke</th>
		<th>Drink</th>
		<th>Bodytype</th>
		<th>Disability</th>
		<th>Country</th>
		<th>State</th>
		<th>City</th>
		<th>Mother Tongue</th>
		<th>About My Partner</th>
		<th>Income</th>
		<th>Actions</th>
    </tr>
	<?php foreach($partnerpreferences as $P){ ?>
    <tr>
		<td><?php echo $P['user_id']; ?></td>
		<td><?php echo $P['agefrom']; ?></td>
		<td><?php echo $P['ageto']; ?></td>
		<td><?php echo $P['heightfrom']; ?></td>
		<td><?php echo $P['heightto']; ?></td>
		<td><?php echo $P['marital_status']; ?></td>
		<td><?php echo $P['health_information']; ?></td>
		<td><?php echo $P['skintone']; ?></td>
		<td><?php echo $P['diet']; ?></td>
		<td><?php echo $P['smoke']; ?></td>
		<td><?php echo $P['drink']; ?></td>
		<td><?php echo $P['bodytype']; ?></td>
		<td><?php echo $P['disability']; ?></td>
		<td><?php echo $P['country']; ?></td>
		<td><?php echo $P['state']; ?></td>
		<td><?php echo $P['city']; ?></td>
		<td><?php echo $P['mother_tongue']; ?></td>
		<td><?php echo $P['about_my_partner']; ?></td>
		<td><?php echo $P['income']; ?></td>
		<td>
            <a href="<?php echo site_url('partnerpreference/edit/'.$P['user_id']); ?>" class="btn btn-info btn-xs">Edit</a> 
            <a href="<?php echo site_url('partnerpreference/remove/'.$P['user_id']); ?>" class="btn btn-danger btn-xs">Delete</a>
        </td>
    </tr>
	<?php } ?>
</table>
