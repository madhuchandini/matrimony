<?php echo form_open('partnerpreference/edit/'.$partnerpreference['user_id'],array("class"=>"form-horizontal")); ?>

	<div class="form-group">
		<label for="agefrom" class="col-md-4 control-label">Agefrom</label>
		<div class="col-md-8">
			<input type="text" name="agefrom" value="<?php echo ($this->input->post('agefrom') ? $this->input->post('agefrom') : $partnerpreference['agefrom']); ?>" class="form-control" id="agefrom" />
		</div>
	</div>
	<div class="form-group">
		<label for="ageto" class="col-md-4 control-label">Ageto</label>
		<div class="col-md-8">
			<input type="text" name="ageto" value="<?php echo ($this->input->post('ageto') ? $this->input->post('ageto') : $partnerpreference['ageto']); ?>" class="form-control" id="ageto" />
		</div>
	</div>
	<div class="form-group">
		<label for="heightfrom" class="col-md-4 control-label">Heightfrom</label>
		<div class="col-md-8">
			<input type="text" name="heightfrom" value="<?php echo ($this->input->post('heightfrom') ? $this->input->post('heightfrom') : $partnerpreference['heightfrom']); ?>" class="form-control" id="heightfrom" />
		</div>
	</div>
	<div class="form-group">
		<label for="heightto" class="col-md-4 control-label">Heightto</label>
		<div class="col-md-8">
			<input type="text" name="heightto" value="<?php echo ($this->input->post('heightto') ? $this->input->post('heightto') : $partnerpreference['heightto']); ?>" class="form-control" id="heightto" />
		</div>
	</div>
	<div class="form-group">
		<label for="marital_status" class="col-md-4 control-label">Marital Status</label>
		<div class="col-md-8">
			<input type="text" name="marital_status" value="<?php echo ($this->input->post('marital_status') ? $this->input->post('marital_status') : $partnerpreference['marital_status']); ?>" class="form-control" id="marital_status" />
		</div>
	</div>
	<div class="form-group">
		<label for="health_information" class="col-md-4 control-label">Health Information</label>
		<div class="col-md-8">
			<input type="text" name="health_information" value="<?php echo ($this->input->post('health_information') ? $this->input->post('health_information') : $partnerpreference['health_information']); ?>" class="form-control" id="health_information" />
		</div>
	</div>
	<div class="form-group">
		<label for="skintone" class="col-md-4 control-label">Skintone</label>
		<div class="col-md-8">
			<input type="text" name="skintone" value="<?php echo ($this->input->post('skintone') ? $this->input->post('skintone') : $partnerpreference['skintone']); ?>" class="form-control" id="skintone" />
		</div>
	</div>
	<div class="form-group">
		<label for="diet" class="col-md-4 control-label">Diet</label>
		<div class="col-md-8">
			<input type="text" name="diet" value="<?php echo ($this->input->post('diet') ? $this->input->post('diet') : $partnerpreference['diet']); ?>" class="form-control" id="diet" />
		</div>
	</div>
	<div class="form-group">
		<label for="smoke" class="col-md-4 control-label">Smoke</label>
		<div class="col-md-8">
			<input type="text" name="smoke" value="<?php echo ($this->input->post('smoke') ? $this->input->post('smoke') : $partnerpreference['smoke']); ?>" class="form-control" id="smoke" />
		</div>
	</div>
	<div class="form-group">
		<label for="drink" class="col-md-4 control-label">Drink</label>
		<div class="col-md-8">
			<input type="text" name="drink" value="<?php echo ($this->input->post('drink') ? $this->input->post('drink') : $partnerpreference['drink']); ?>" class="form-control" id="drink" />
		</div>
	</div>
	<div class="form-group">
		<label for="bodytype" class="col-md-4 control-label">Bodytype</label>
		<div class="col-md-8">
			<input type="text" name="bodytype" value="<?php echo ($this->input->post('bodytype') ? $this->input->post('bodytype') : $partnerpreference['bodytype']); ?>" class="form-control" id="bodytype" />
		</div>
	</div>
	<div class="form-group">
		<label for="disability" class="col-md-4 control-label">Disability</label>
		<div class="col-md-8">
			<input type="text" name="disability" value="<?php echo ($this->input->post('disability') ? $this->input->post('disability') : $partnerpreference['disability']); ?>" class="form-control" id="disability" />
		</div>
	</div>
	<div class="form-group">
		<label for="country" class="col-md-4 control-label">Country</label>
		<div class="col-md-8">
			<input type="text" name="country" value="<?php echo ($this->input->post('country') ? $this->input->post('country') : $partnerpreference['country']); ?>" class="form-control" id="country" />
		</div>
	</div>
	<div class="form-group">
		<label for="state" class="col-md-4 control-label">State</label>
		<div class="col-md-8">
			<input type="text" name="state" value="<?php echo ($this->input->post('state') ? $this->input->post('state') : $partnerpreference['state']); ?>" class="form-control" id="state" />
		</div>
	</div>
	<div class="form-group">
		<label for="city" class="col-md-4 control-label">City</label>
		<div class="col-md-8">
			<input type="text" name="city" value="<?php echo ($this->input->post('city') ? $this->input->post('city') : $partnerpreference['city']); ?>" class="form-control" id="city" />
		</div>
	</div>
	<div class="form-group">
		<label for="mother_tongue" class="col-md-4 control-label">Mother Tongue</label>
		<div class="col-md-8">
			<input type="text" name="mother_tongue" value="<?php echo ($this->input->post('mother_tongue') ? $this->input->post('mother_tongue') : $partnerpreference['mother_tongue']); ?>" class="form-control" id="mother_tongue" />
		</div>
	</div>
	<div class="form-group">
		<label for="about_my_partner" class="col-md-4 control-label">About My Partner</label>
		<div class="col-md-8">
			<textarea name="about_my_partner" class="form-control" id="about_my_partner"><?php echo ($this->input->post('about_my_partner') ? $this->input->post('about_my_partner') : $partnerpreference['about_my_partner']); ?></textarea>
		</div>
	</div>
	<div class="form-group">
		<label for="income" class="col-md-4 control-label">Income</label>
		<div class="col-md-8">
			<textarea name="income" class="form-control" id="income"><?php echo ($this->input->post('income') ? $this->input->post('income') : $partnerpreference['income']); ?></textarea>
		</div>
	</div>
	
	<div class="form-group">
		<div class="col-sm-offset-4 col-sm-8">
			<button type="submit" class="btn btn-success">Save</button>
        </div>
	</div>
	
<?php echo form_close(); ?>