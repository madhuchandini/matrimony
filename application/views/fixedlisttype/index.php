<div class="pull-right">
	<a href="<?php echo site_url('fixedlisttype/add'); ?>" class="btn btn-success">Add</a> 
</div>

<table class="table table-striped table-bordered">
    <tr>
		<th>ID</th>
		<th>Name</th>
		<th>Actions</th>
    </tr>
	<?php foreach($fixedlisttypes as $F){ ?>
    <tr>
		<td><?php echo $F['id']; ?></td>
		<td><?php echo $F['name']; ?></td>
		<td>
            <a href="<?php echo site_url('fixedlisttype/edit/'.$F['id']); ?>" class="btn btn-info btn-xs">Edit</a> 
            <a href="<?php echo site_url('fixedlisttype/remove/'.$F['id']); ?>" class="btn btn-danger btn-xs">Delete</a>
        </td>
    </tr>
	<?php } ?>
</table>
