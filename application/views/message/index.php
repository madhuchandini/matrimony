<div class="pull-right">
	<a href="<?php echo site_url('message/add'); ?>" class="btn btn-success">Add</a> 
</div>

<table class="table table-striped table-bordered">
    <tr>
		<th>ID</th>
		<th>Sender Id</th>
		<th>Receiver Id</th>
		<th>Subject</th>
		<th>Message</th>
		<th>Actions</th>
    </tr>
	<?php foreach($messages as $M){ ?>
    <tr>
		<td><?php echo $M['id']; ?></td>
		<td><?php echo $M['sender_id']; ?></td>
		<td><?php echo $M['receiver_id']; ?></td>
		<td><?php echo $M['subject']; ?></td>
		<td><?php echo $M['message']; ?></td>
		<td>
            <a href="<?php echo site_url('message/edit/'.$M['id']); ?>" class="btn btn-info btn-xs">Edit</a> 
            <a href="<?php echo site_url('message/remove/'.$M['id']); ?>" class="btn btn-danger btn-xs">Delete</a>
        </td>
    </tr>
	<?php } ?>
</table>
