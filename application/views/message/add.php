<?php echo form_open('message/add',array("class"=>"form-horizontal")); ?>

	<div class="form-group">
		<label for="sender_id" class="col-md-4 control-label">Sender Id</label>
		<div class="col-md-8">
			<input type="text" name="sender_id" value="<?php echo $this->input->post('sender_id'); ?>" class="form-control" id="sender_id" />
		</div>
	</div>
	<div class="form-group">
		<label for="receiver_id" class="col-md-4 control-label">Receiver Id</label>
		<div class="col-md-8">
			<input type="text" name="receiver_id" value="<?php echo $this->input->post('receiver_id'); ?>" class="form-control" id="receiver_id" />
		</div>
	</div>
	<div class="form-group">
		<label for="subject" class="col-md-4 control-label">Subject</label>
		<div class="col-md-8">
			<textarea name="subject" class="form-control" id="subject"><?php echo $this->input->post('subject'); ?></textarea>
		</div>
	</div>
	<div class="form-group">
		<label for="message" class="col-md-4 control-label">Message</label>
		<div class="col-md-8">
			<textarea name="message" class="form-control" id="message"><?php echo $this->input->post('message'); ?></textarea>
		</div>
	</div>
	
	<div class="form-group">
		<div class="col-sm-offset-4 col-sm-8">
			<button type="submit" class="btn btn-success">Save</button>
        </div>
	</div>

<?php echo form_close(); ?>