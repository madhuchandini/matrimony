<div class="pull-right">
	<a href="<?php echo site_url('messagetemplate/add'); ?>" class="btn btn-success">Add</a> 
</div>

<table class="table table-striped table-bordered">
    <tr>
		<th>ID</th>
		<th>Name</th>
		<th>Text</th>
		<th>Actions</th>
    </tr>
	<?php foreach($messagetemplates as $M){ ?>
    <tr>
		<td><?php echo $M['id']; ?></td>
		<td><?php echo $M['name']; ?></td>
		<td><?php echo $M['text']; ?></td>
		<td>
            <a href="<?php echo site_url('messagetemplate/edit/'.$M['id']); ?>" class="btn btn-info btn-xs">Edit</a> 
            <a href="<?php echo site_url('messagetemplate/remove/'.$M['id']); ?>" class="btn btn-danger btn-xs">Delete</a>
        </td>
    </tr>
	<?php } ?>
</table>
