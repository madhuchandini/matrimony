<div class="pull-right">
	<a href="<?php echo site_url('userprofile/add'); ?>" class="btn btn-success">Add</a> 
</div>

<table class="table table-striped table-bordered">
    <tr>
		<th>User Id</th>
		<th>Gender</th>
		<th>Age</th>
		<th>Marital Status</th>
		<th>Country</th>
		<th>State</th>
		<th>City</th>
		<th>Mothertongue</th>
		<th>Height</th>
		<th>Skintone</th>
		<th>Bodytype</th>
		<th>Disability</th>
		<th>Diet</th>
		<th>Smoke</th>
		<th>Drink</th>
		<th>Religion</th>
		<th>Caste</th>
		<th>Sub Caste</th>
		<th>Highest Education</th>
		<th>Education Field</th>
		<th>Occupation</th>
		<th>Income Currency</th>
		<th>Father Name</th>
		<th>Mother Name</th>
		<th>Family Status</th>
		<th>Married Brothers</th>
		<th>Married Sisters</th>
		<th>Hobbies</th>
		<th>Interests</th>
		<th>Family Type</th>
		<th>Open To Other Castes</th>
		<th>Family Values</th>
		<th>Annual Income Inr</th>
		<th>Annual Income Other</th>
		<th>Date Of Birth</th>
		<th>Unmarried Brothers</th>
		<th>Unmarried Sisters</th>
		<th>Favourite Music</th>
		<th>Preferred Movies</th>
		<th>Food I Cook</th>
		<th>In Your Own Words</th>
		<th>Photos</th>
		<th>Actions</th>
    </tr>
	<?php foreach($userprofiles as $U){ ?>
    <tr>
		<td><?php echo $U['user_id']; ?></td>
		<td><?php echo $U['gender']; ?></td>
		<td><?php echo $U['age']; ?></td>
		<td><?php echo $U['marital_status']; ?></td>
		<td><?php echo $U['country']; ?></td>
		<td><?php echo $U['state']; ?></td>
		<td><?php echo $U['city']; ?></td>
		<td><?php echo $U['mothertongue']; ?></td>
		<td><?php echo $U['height']; ?></td>
		<td><?php echo $U['skintone']; ?></td>
		<td><?php echo $U['bodytype']; ?></td>
		<td><?php echo $U['disability']; ?></td>
		<td><?php echo $U['diet']; ?></td>
		<td><?php echo $U['smoke']; ?></td>
		<td><?php echo $U['drink']; ?></td>
		<td><?php echo $U['religion']; ?></td>
		<td><?php echo $U['caste']; ?></td>
		<td><?php echo $U['sub_caste']; ?></td>
		<td><?php echo $U['highest_education']; ?></td>
		<td><?php echo $U['education_field']; ?></td>
		<td><?php echo $U['occupation']; ?></td>
		<td><?php echo $U['income_currency']; ?></td>
		<td><?php echo $U['father_name']; ?></td>
		<td><?php echo $U['mother_name']; ?></td>
		<td><?php echo $U['family_status']; ?></td>
		<td><?php echo $U['married_brothers']; ?></td>
		<td><?php echo $U['married_sisters']; ?></td>
		<td><?php echo $U['hobbies']; ?></td>
		<td><?php echo $U['interests']; ?></td>
		<td><?php echo $U['family_type']; ?></td>
		<td><?php echo $U['open_to_other_castes']; ?></td>
		<td><?php echo $U['family_values']; ?></td>
		<td><?php echo $U['annual_income_inr']; ?></td>
		<td><?php echo $U['annual_income_other']; ?></td>
		<td><?php echo $U['date_of_birth']; ?></td>
		<td><?php echo $U['unmarried_brothers']; ?></td>
		<td><?php echo $U['unmarried_sisters']; ?></td>
		<td><?php echo $U['favourite_music']; ?></td>
		<td><?php echo $U['preferred_movies']; ?></td>
		<td><?php echo $U['food_i_cook']; ?></td>
		<td><?php echo $U['in_your_own_words']; ?></td>
		<td><?php echo $U['photos']; ?></td>
		<td>
            <a href="<?php echo site_url('userprofile/edit/'.$U['user_id']); ?>" class="btn btn-info btn-xs">Edit</a> 
            <a href="<?php echo site_url('userprofile/remove/'.$U['user_id']); ?>" class="btn btn-danger btn-xs">Delete</a>
        </td>
    </tr>
	<?php } ?>
</table>
