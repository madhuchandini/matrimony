<?php echo form_open('userprofile/add',array("class"=>"form-horizontal")); ?>

	<div class="form-group">
		<label for="gender" class="col-md-4 control-label">Gender</label>
		<div class="col-md-8">
			<input type="text" name="gender" value="<?php echo $this->input->post('gender'); ?>" class="form-control" id="gender" />
		</div>
	</div>
	<div class="form-group">
		<label for="age" class="col-md-4 control-label">Age</label>
		<div class="col-md-8">
			<input type="text" name="age" value="<?php echo $this->input->post('age'); ?>" class="form-control" id="age" />
		</div>
	</div>
	<div class="form-group">
		<label for="marital_status" class="col-md-4 control-label">Marital Status</label>
		<div class="col-md-8">
			<input type="text" name="marital_status" value="<?php echo $this->input->post('marital_status'); ?>" class="form-control" id="marital_status" />
		</div>
	</div>
	<div class="form-group">
		<label for="country" class="col-md-4 control-label">Country</label>
		<div class="col-md-8">
			<input type="text" name="country" value="<?php echo $this->input->post('country'); ?>" class="form-control" id="country" />
		</div>
	</div>
	<div class="form-group">
		<label for="state" class="col-md-4 control-label">State</label>
		<div class="col-md-8">
			<input type="text" name="state" value="<?php echo $this->input->post('state'); ?>" class="form-control" id="state" />
		</div>
	</div>
	<div class="form-group">
		<label for="city" class="col-md-4 control-label">City</label>
		<div class="col-md-8">
			<input type="text" name="city" value="<?php echo $this->input->post('city'); ?>" class="form-control" id="city" />
		</div>
	</div>
	<div class="form-group">
		<label for="mothertongue" class="col-md-4 control-label">Mothertongue</label>
		<div class="col-md-8">
			<input type="text" name="mothertongue" value="<?php echo $this->input->post('mothertongue'); ?>" class="form-control" id="mothertongue" />
		</div>
	</div>
	<div class="form-group">
		<label for="height" class="col-md-4 control-label">Height</label>
		<div class="col-md-8">
			<input type="text" name="height" value="<?php echo $this->input->post('height'); ?>" class="form-control" id="height" />
		</div>
	</div>
	<div class="form-group">
		<label for="skintone" class="col-md-4 control-label">Skintone</label>
		<div class="col-md-8">
			<input type="text" name="skintone" value="<?php echo $this->input->post('skintone'); ?>" class="form-control" id="skintone" />
		</div>
	</div>
	<div class="form-group">
		<label for="bodytype" class="col-md-4 control-label">Bodytype</label>
		<div class="col-md-8">
			<input type="text" name="bodytype" value="<?php echo $this->input->post('bodytype'); ?>" class="form-control" id="bodytype" />
		</div>
	</div>
	<div class="form-group">
		<label for="disability" class="col-md-4 control-label">Disability</label>
		<div class="col-md-8">
			<input type="text" name="disability" value="<?php echo $this->input->post('disability'); ?>" class="form-control" id="disability" />
		</div>
	</div>
	<div class="form-group">
		<label for="diet" class="col-md-4 control-label">Diet</label>
		<div class="col-md-8">
			<input type="text" name="diet" value="<?php echo $this->input->post('diet'); ?>" class="form-control" id="diet" />
		</div>
	</div>
	<div class="form-group">
		<label for="smoke" class="col-md-4 control-label">Smoke</label>
		<div class="col-md-8">
			<input type="text" name="smoke" value="<?php echo $this->input->post('smoke'); ?>" class="form-control" id="smoke" />
		</div>
	</div>
	<div class="form-group">
		<label for="drink" class="col-md-4 control-label">Drink</label>
		<div class="col-md-8">
			<input type="text" name="drink" value="<?php echo $this->input->post('drink'); ?>" class="form-control" id="drink" />
		</div>
	</div>
	<div class="form-group">
		<label for="religion" class="col-md-4 control-label">Religion</label>
		<div class="col-md-8">
			<input type="text" name="religion" value="<?php echo $this->input->post('religion'); ?>" class="form-control" id="religion" />
		</div>
	</div>
	<div class="form-group">
		<label for="caste" class="col-md-4 control-label">Caste</label>
		<div class="col-md-8">
			<input type="text" name="caste" value="<?php echo $this->input->post('caste'); ?>" class="form-control" id="caste" />
		</div>
	</div>
	<div class="form-group">
		<label for="sub_caste" class="col-md-4 control-label">Sub Caste</label>
		<div class="col-md-8">
			<input type="text" name="sub_caste" value="<?php echo $this->input->post('sub_caste'); ?>" class="form-control" id="sub_caste" />
		</div>
	</div>
	<div class="form-group">
		<label for="highest_education" class="col-md-4 control-label">Highest Education</label>
		<div class="col-md-8">
			<input type="text" name="highest_education" value="<?php echo $this->input->post('highest_education'); ?>" class="form-control" id="highest_education" />
		</div>
	</div>
	<div class="form-group">
		<label for="education_field" class="col-md-4 control-label">Education Field</label>
		<div class="col-md-8">
			<input type="text" name="education_field" value="<?php echo $this->input->post('education_field'); ?>" class="form-control" id="education_field" />
		</div>
	</div>
	<div class="form-group">
		<label for="occupation" class="col-md-4 control-label">Occupation</label>
		<div class="col-md-8">
			<input type="text" name="occupation" value="<?php echo $this->input->post('occupation'); ?>" class="form-control" id="occupation" />
		</div>
	</div>
	<div class="form-group">
		<label for="income_currency" class="col-md-4 control-label">Income Currency</label>
		<div class="col-md-8">
			<input type="text" name="income_currency" value="<?php echo $this->input->post('income_currency'); ?>" class="form-control" id="income_currency" />
		</div>
	</div>
	<div class="form-group">
		<label for="father_name" class="col-md-4 control-label">Father Name</label>
		<div class="col-md-8">
			<input type="text" name="father_name" value="<?php echo $this->input->post('father_name'); ?>" class="form-control" id="father_name" />
		</div>
	</div>
	<div class="form-group">
		<label for="mother_name" class="col-md-4 control-label">Mother Name</label>
		<div class="col-md-8">
			<input type="text" name="mother_name" value="<?php echo $this->input->post('mother_name'); ?>" class="form-control" id="mother_name" />
		</div>
	</div>
	<div class="form-group">
		<label for="family_status" class="col-md-4 control-label">Family Status</label>
		<div class="col-md-8">
			<input type="text" name="family_status" value="<?php echo $this->input->post('family_status'); ?>" class="form-control" id="family_status" />
		</div>
	</div>
	<div class="form-group">
		<label for="married_brothers" class="col-md-4 control-label">Married Brothers</label>
		<div class="col-md-8">
			<input type="text" name="married_brothers" value="<?php echo $this->input->post('married_brothers'); ?>" class="form-control" id="married_brothers" />
		</div>
	</div>
	<div class="form-group">
		<label for="married_sisters" class="col-md-4 control-label">Married Sisters</label>
		<div class="col-md-8">
			<input type="text" name="married_sisters" value="<?php echo $this->input->post('married_sisters'); ?>" class="form-control" id="married_sisters" />
		</div>
	</div>
	<div class="form-group">
		<label for="hobbies" class="col-md-4 control-label">Hobbies</label>
		<div class="col-md-8">
			<input type="text" name="hobbies" value="<?php echo $this->input->post('hobbies'); ?>" class="form-control" id="hobbies" />
		</div>
	</div>
	<div class="form-group">
		<label for="interests" class="col-md-4 control-label">Interests</label>
		<div class="col-md-8">
			<input type="text" name="interests" value="<?php echo $this->input->post('interests'); ?>" class="form-control" id="interests" />
		</div>
	</div>
	<div class="form-group">
		<label for="family_type" class="col-md-4 control-label">Family Type</label>
		<div class="col-md-8">
			<input type="text" name="family_type" value="<?php echo $this->input->post('family_type'); ?>" class="form-control" id="family_type" />
		</div>
	</div>
	<div class="form-group">
		<label for="open_to_other_castes" class="col-md-4 control-label">Open To Other Castes</label>
		<div class="col-md-8">
			<input type="text" name="open_to_other_castes" value="<?php echo $this->input->post('open_to_other_castes'); ?>" class="form-control" id="open_to_other_castes" />
		</div>
	</div>
	<div class="form-group">
		<label for="family_values" class="col-md-4 control-label">Family Values</label>
		<div class="col-md-8">
			<input type="text" name="family_values" value="<?php echo $this->input->post('family_values'); ?>" class="form-control" id="family_values" />
		</div>
	</div>
	<div class="form-group">
		<label for="annual_income_inr" class="col-md-4 control-label">Annual Income Inr</label>
		<div class="col-md-8">
			<input type="text" name="annual_income_inr" value="<?php echo $this->input->post('annual_income_inr'); ?>" class="form-control" id="annual_income_inr" />
		</div>
	</div>
	<div class="form-group">
		<label for="annual_income_other" class="col-md-4 control-label">Annual Income Other</label>
		<div class="col-md-8">
			<input type="text" name="annual_income_other" value="<?php echo $this->input->post('annual_income_other'); ?>" class="form-control" id="annual_income_other" />
		</div>
	</div>
	<div class="form-group">
		<label for="date_of_birth" class="col-md-4 control-label">Date Of Birth</label>
		<div class="col-md-8">
			<input type="text" name="date_of_birth" value="<?php echo $this->input->post('date_of_birth'); ?>" class="form-control" id="date_of_birth" />
		</div>
	</div>
	<div class="form-group">
		<label for="unmarried_brothers" class="col-md-4 control-label">Unmarried Brothers</label>
		<div class="col-md-8">
			<input type="text" name="unmarried_brothers" value="<?php echo $this->input->post('unmarried_brothers'); ?>" class="form-control" id="unmarried_brothers" />
		</div>
	</div>
	<div class="form-group">
		<label for="unmarried_sisters" class="col-md-4 control-label">Unmarried Sisters</label>
		<div class="col-md-8">
			<input type="text" name="unmarried_sisters" value="<?php echo $this->input->post('unmarried_sisters'); ?>" class="form-control" id="unmarried_sisters" />
		</div>
	</div>
	<div class="form-group">
		<label for="favourite_music" class="col-md-4 control-label">Favourite Music</label>
		<div class="col-md-8">
			<textarea name="favourite_music" class="form-control" id="favourite_music"><?php echo $this->input->post('favourite_music'); ?></textarea>
		</div>
	</div>
	<div class="form-group">
		<label for="preferred_movies" class="col-md-4 control-label">Preferred Movies</label>
		<div class="col-md-8">
			<textarea name="preferred_movies" class="form-control" id="preferred_movies"><?php echo $this->input->post('preferred_movies'); ?></textarea>
		</div>
	</div>
	<div class="form-group">
		<label for="food_i_cook" class="col-md-4 control-label">Food I Cook</label>
		<div class="col-md-8">
			<textarea name="food_i_cook" class="form-control" id="food_i_cook"><?php echo $this->input->post('food_i_cook'); ?></textarea>
		</div>
	</div>
	<div class="form-group">
		<label for="in_your_own_words" class="col-md-4 control-label">In Your Own Words</label>
		<div class="col-md-8">
			<textarea name="in_your_own_words" class="form-control" id="in_your_own_words"><?php echo $this->input->post('in_your_own_words'); ?></textarea>
		</div>
	</div>
	<div class="form-group">
		<label for="photos" class="col-md-4 control-label">Photos</label>
		<div class="col-md-8">
			<textarea name="photos" class="form-control" id="photos"><?php echo $this->input->post('photos'); ?></textarea>
		</div>
	</div>
	
	<div class="form-group">
		<div class="col-sm-offset-4 col-sm-8">
			<button type="submit" class="btn btn-success">Save</button>
        </div>
	</div>

<?php echo form_close(); ?>