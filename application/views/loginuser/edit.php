<?php echo form_open('loginuser/edit/'.$loginuser['id'],array("class"=>"form-horizontal")); ?>

	<div class="form-group">
		<label for="password" class="col-md-4 control-label">Password</label>
		<div class="col-md-8">
			<input type="text" name="password" value="<?php echo ($this->input->post('password') ? $this->input->post('password') : $loginuser['password']); ?>" class="form-control" id="password" />
		</div>
	</div>
	<div class="form-group">
		<label for="createdFor" class="col-md-4 control-label">CreatedFor</label>
		<div class="col-md-8">
			<input type="text" name="createdFor" value="<?php echo ($this->input->post('createdFor') ? $this->input->post('createdFor') : $loginuser['createdFor']); ?>" class="form-control" id="createdFor" />
		</div>
	</div>
	<div class="form-group">
		<label for="emailId" class="col-md-4 control-label">EmailId</label>
		<div class="col-md-8">
			<input type="text" name="emailId" value="<?php echo ($this->input->post('emailId') ? $this->input->post('emailId') : $loginuser['emailId']); ?>" class="form-control" id="emailId" />
		</div>
	</div>
	<div class="form-group">
		<label for="mobileNo" class="col-md-4 control-label">MobileNo</label>
		<div class="col-md-8">
			<input type="text" name="mobileNo" value="<?php echo ($this->input->post('mobileNo') ? $this->input->post('mobileNo') : $loginuser['mobileNo']); ?>" class="form-control" id="mobileNo" />
		</div>
	</div>
	<div class="form-group">
		<label for="userType" class="col-md-4 control-label">UserType</label>
		<div class="col-md-8">
			<input type="text" name="userType" value="<?php echo ($this->input->post('userType') ? $this->input->post('userType') : $loginuser['userType']); ?>" class="form-control" id="userType" />
		</div>
	</div>
	<div class="form-group">
		<label for="resetPasswordKey" class="col-md-4 control-label">ResetPasswordKey</label>
		<div class="col-md-8">
			<input type="text" name="resetPasswordKey" value="<?php echo ($this->input->post('resetPasswordKey') ? $this->input->post('resetPasswordKey') : $loginuser['resetPasswordKey']); ?>" class="form-control" id="resetPasswordKey" />
		</div>
	</div>
	<div class="form-group">
		<label for="authenticationKey" class="col-md-4 control-label">AuthenticationKey</label>
		<div class="col-md-8">
			<input type="text" name="authenticationKey" value="<?php echo ($this->input->post('authenticationKey') ? $this->input->post('authenticationKey') : $loginuser['authenticationKey']); ?>" class="form-control" id="authenticationKey" />
		</div>
	</div>
	
	<div class="form-group">
		<div class="col-sm-offset-4 col-sm-8">
			<button type="submit" class="btn btn-success">Save</button>
        </div>
	</div>
	
<?php echo form_close(); ?>