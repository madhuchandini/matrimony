<div class="pull-right">
	<a href="<?php echo site_url('loginuser/add'); ?>" class="btn btn-success">Add</a> 
</div>

<table class="table table-striped table-bordered">
    <tr>
		<th>ID</th>
		<th>Password</th>
		<th>CreatedFor</th>
		<th>EmailId</th>
		<th>MobileNo</th>
		<th>UserType</th>
		<th>ResetPasswordKey</th>
		<th>AuthenticationKey</th>
		<th>Actions</th>
    </tr>
	<?php foreach($loginusers as $L){ ?>
    <tr>
		<td><?php echo $L['id']; ?></td>
		<td><?php echo $L['password']; ?></td>
		<td><?php echo $L['createdFor']; ?></td>
		<td><?php echo $L['emailId']; ?></td>
		<td><?php echo $L['mobileNo']; ?></td>
		<td><?php echo $L['userType']; ?></td>
		<td><?php echo $L['resetPasswordKey']; ?></td>
		<td><?php echo $L['authenticationKey']; ?></td>
		<td>
            <a href="<?php echo site_url('loginuser/edit/'.$L['id']); ?>" class="btn btn-info btn-xs">Edit</a> 
            <a href="<?php echo site_url('loginuser/remove/'.$L['id']); ?>" class="btn btn-danger btn-xs">Delete</a>
        </td>
    </tr>
	<?php } ?>
</table>
