<div class="pull-right">
	<a href="<?php echo site_url('savedprofile/add'); ?>" class="btn btn-success">Add</a> 
</div>

<table class="table table-striped table-bordered">
    <tr>
		<th>UserId</th>
		<th>SavedProfile</th>
		<th>Actions</th>
    </tr>
	<?php foreach($savedprofiles as $S){ ?>
    <tr>
		<td><?php echo $S['userId']; ?></td>
		<td><?php echo $S['savedProfile']; ?></td>
		<td>
            <a href="<?php echo site_url('savedprofile/edit/'.$S['userId']); ?>" class="btn btn-info btn-xs">Edit</a> 
            <a href="<?php echo site_url('savedprofile/remove/'.$S['userId']); ?>" class="btn btn-danger btn-xs">Delete</a>
        </td>
    </tr>
	<?php } ?>
</table>
