<?php
/* 
 * Generated by CRUDigniter v3.2 
 * www.crudigniter.com
 */
 
class Loginuser extends CI_Controller{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Loginuser_model');
    } 

    /*
     * Listing of loginusers
     */
    function index()
    {
        $data['loginusers'] = $this->Loginuser_model->get_all_loginusers();
        
        $data['_view'] = 'loginuser/index';
        $this->load->view('layouts/main',$data);
    }

    /*
     * Adding a new loginuser
     */
    function add()
    {   
        if(isset($_POST) && count($_POST) > 0)     
        {   
            $params = array(
				'password' => $this->input->post('password'),
				'createdFor' => $this->input->post('createdFor'),
				'emailId' => $this->input->post('emailId'),
				'mobileNo' => $this->input->post('mobileNo'),
				'userType' => $this->input->post('userType'),
				'resetPasswordKey' => $this->input->post('resetPasswordKey'),
				'authenticationKey' => $this->input->post('authenticationKey'),
            );
            
            $loginuser_id = $this->Loginuser_model->add_loginuser($params);
            redirect('loginuser/index');
        }
        else
        {            
            $data['_view'] = 'loginuser/add';
            $this->load->view('layouts/main',$data);
        }
    }  

    /*
     * Editing a loginuser
     */
    function edit($id)
    {   
        // check if the loginuser exists before trying to edit it
        $data['loginuser'] = $this->Loginuser_model->get_loginuser($id);
        
        if(isset($data['loginuser']['id']))
        {
            if(isset($_POST) && count($_POST) > 0)     
            {   
                $params = array(
					'password' => $this->input->post('password'),
					'createdFor' => $this->input->post('createdFor'),
					'emailId' => $this->input->post('emailId'),
					'mobileNo' => $this->input->post('mobileNo'),
					'userType' => $this->input->post('userType'),
					'resetPasswordKey' => $this->input->post('resetPasswordKey'),
					'authenticationKey' => $this->input->post('authenticationKey'),
                );

                $this->Loginuser_model->update_loginuser($id,$params);            
                redirect('loginuser/index');
            }
            else
            {
                $data['_view'] = 'loginuser/edit';
                $this->load->view('layouts/main',$data);
            }
        }
        else
            show_error('The loginuser you are trying to edit does not exist.');
    } 

    /*
     * Deleting loginuser
     */
    function remove($id)
    {
        $loginuser = $this->Loginuser_model->get_loginuser($id);

        // check if the loginuser exists before trying to delete it
        if(isset($loginuser['id']))
        {
            $this->Loginuser_model->delete_loginuser($id);
            redirect('loginuser/index');
        }
        else
            show_error('The loginuser you are trying to delete does not exist.');
    }
    
}
