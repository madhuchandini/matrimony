<?php
/* 
 * Generated by CRUDigniter v3.2 
 * www.crudigniter.com
 */
 
class Messagetemplate extends CI_Controller{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Messagetemplate_model');
    } 

    /*
     * Listing of messagetemplates
     */
    function index()
    {
        $data['messagetemplates'] = $this->Messagetemplate_model->get_all_messagetemplates();
        
        $data['_view'] = 'messagetemplate/index';
        $this->load->view('layouts/main',$data);
    }

    /*
     * Adding a new messagetemplate
     */
    function add()
    {   
        if(isset($_POST) && count($_POST) > 0)     
        {   
            $params = array(
				'name' => $this->input->post('name'),
				'text' => $this->input->post('text'),
            );
            
            $messagetemplate_id = $this->Messagetemplate_model->add_messagetemplate($params);
            redirect('messagetemplate/index');
        }
        else
        {            
            $data['_view'] = 'messagetemplate/add';
            $this->load->view('layouts/main',$data);
        }
    }  

    /*
     * Editing a messagetemplate
     */
    function edit($id)
    {   
        // check if the messagetemplate exists before trying to edit it
        $data['messagetemplate'] = $this->Messagetemplate_model->get_messagetemplate($id);
        
        if(isset($data['messagetemplate']['id']))
        {
            if(isset($_POST) && count($_POST) > 0)     
            {   
                $params = array(
					'name' => $this->input->post('name'),
					'text' => $this->input->post('text'),
                );

                $this->Messagetemplate_model->update_messagetemplate($id,$params);            
                redirect('messagetemplate/index');
            }
            else
            {
                $data['_view'] = 'messagetemplate/edit';
                $this->load->view('layouts/main',$data);
            }
        }
        else
            show_error('The messagetemplate you are trying to edit does not exist.');
    } 

    /*
     * Deleting messagetemplate
     */
    function remove($id)
    {
        $messagetemplate = $this->Messagetemplate_model->get_messagetemplate($id);

        // check if the messagetemplate exists before trying to delete it
        if(isset($messagetemplate['id']))
        {
            $this->Messagetemplate_model->delete_messagetemplate($id);
            redirect('messagetemplate/index');
        }
        else
            show_error('The messagetemplate you are trying to delete does not exist.');
    }
    
}
