<?php
/* 
 * Generated by CRUDigniter v3.2 
 * www.crudigniter.com
 */
 
class City extends CI_Controller{
    function __construct()
    {
        parent::__construct();
        $this->load->model('City_model');
    } 

    /*
     * Listing of cities
     */
    function index()
    {
        $params['limit'] = RECORDS_PER_PAGE; 
        $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        
        $config = $this->config->item('pagination');
        $config['base_url'] = site_url('city/index?');
        $config['total_rows'] = $this->City_model->get_all_cities_count();
        $this->pagination->initialize($config);

        $data['cities'] = $this->City_model->get_all_cities($params);
        
        $data['_view'] = 'city/index';
        $this->load->view('layouts/main',$data);
    }

    /*
     * Adding a new city
     */
    function add()
    {   
        if(isset($_POST) && count($_POST) > 0)     
        {   
            $params = array(
				'state' => $this->input->post('state'),
				'name' => $this->input->post('name'),
            );
            
            $city_id = $this->City_model->add_city($params);
            redirect('city/index');
        }
        else
        {            
            $data['_view'] = 'city/add';
            $this->load->view('layouts/main',$data);
        }
    }  

    /*
     * Editing a city
     */
    function edit($id)
    {   
        // check if the city exists before trying to edit it
        $data['city'] = $this->City_model->get_city($id);
        
        if(isset($data['city']['id']))
        {
            if(isset($_POST) && count($_POST) > 0)     
            {   
                $params = array(
					'state' => $this->input->post('state'),
					'name' => $this->input->post('name'),
                );

                $this->City_model->update_city($id,$params);            
                redirect('city/index');
            }
            else
            {
                $data['_view'] = 'city/edit';
                $this->load->view('layouts/main',$data);
            }
        }
        else
            show_error('The city you are trying to edit does not exist.');
    } 

    /*
     * Deleting city
     */
    function remove($id)
    {
        $city = $this->City_model->get_city($id);

        // check if the city exists before trying to delete it
        if(isset($city['id']))
        {
            $this->City_model->delete_city($id);
            redirect('city/index');
        }
        else
            show_error('The city you are trying to delete does not exist.');
    }
    
}
