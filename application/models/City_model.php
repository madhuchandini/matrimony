<?php
/* 
 * Generated by CRUDigniter v3.2 
 * www.crudigniter.com
 */
 
class City_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get city by id
     */
    function get_city($id)
    {
        return $this->db->get_where('Cities',array('id'=>$id))->row_array();
    }
    
    /*
     * Get all cities count
     */
    function get_all_cities_count()
    {
        $this->db->from('Cities');
        return $this->db->count_all_results();
    }
        
    /*
     * Get all cities
     */
    function get_all_cities($params = array())
    {
        $this->db->order_by('id', 'desc');
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }
        return $this->db->get('Cities')->result_array();
    }
        
    /*
     * function to add new city
     */
    function add_city($params)
    {
        $this->db->insert('Cities',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update city
     */
    function update_city($id,$params)
    {
        $this->db->where('id',$id);
        return $this->db->update('Cities',$params);
    }
    
    /*
     * function to delete city
     */
    function delete_city($id)
    {
        return $this->db->delete('Cities',array('id'=>$id));
    }
}
